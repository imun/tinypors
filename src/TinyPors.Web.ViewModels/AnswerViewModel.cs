﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using TinyPors.Web.ViewModels.Base;
using TinyPors.Web.Resources;
namespace TinyPors.Web.ViewModels {
    public class AnswerViewModel: BaseViewModel {
        #region Db Fields
        [Key][HiddenInput]
        public int Id { get; set; }

        [HiddenInput]
        public int QuestionId { get; set; }

        [Display(ResourceType = typeof(ModelNames), Name = "Answer_Body")]
        [Required(ErrorMessageResourceType = typeof(ModelErrors), ErrorMessageResourceName = "Required")]
        [StringLength(2000, ErrorMessageResourceType = typeof(ModelErrors), ErrorMessageResourceName = "StringLen")]
        public string Body { get; set; }

        public bool IsCorrect { get; set; }

        public int UserId { get; set; }
        #endregion
        #region Collections

        #endregion
    }
}
