﻿using System.ComponentModel.DataAnnotations;
using TinyPors.Core.Domain.Base;
using TinyPors.Web.Resources;
using TinyPors.Web.ViewModels.Base;

namespace TinyPors.Web.ViewModels {
    public class QuestionViewModel: BaseViewModel {
        #region Db Fields
        [Key]
        public int Id { get; set; }

        [Display(ResourceType = typeof(ModelNames), Name = "Question_Title")]
        [StringLength(1000, ErrorMessageResourceType = typeof(ModelErrors), ErrorMessageResourceName = "StringLen")]
        public string Title { get; set; }

        [Display(ResourceType = typeof(ModelNames), Name = "Question_Body")]
        [Required(ErrorMessageResourceType = typeof(ModelErrors), ErrorMessageResourceName = "Required")]
        [StringLength(4000, ErrorMessageResourceType = typeof(ModelErrors), ErrorMessageResourceName = "StringLen")]
        public string Body { get; set; }

        public int UserId { get; set; }

        [Display(ResourceType = typeof(ModelNames), Name = "Question_MultiSelect")]
        public bool MultiSelect { get; set; }

        [Display(ResourceType = typeof(ModelNames), Name = "Question_DuplicateIp")]
        public bool DuplicateIp { get; set; }

        [Display(ResourceType = typeof(ModelNames), Name = "Question_ImagePath")]
        public string ImagePath { get; set; }

        public QuestionType QuestionType { get; set; } = QuestionType.MultiWithOneAnswer;

        public string Ip { get; set; }

        #endregion
    }
}
