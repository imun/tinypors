﻿namespace TinyPors.Web.ViewModels.Base {
    public abstract class BaseViewModel {
        public string ModelMessage { get; set; }
        public bool Success { get; set; }
        public bool HasError { get; set; }
        public string ErrorFieldName { get; set; }
    }
}
