﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using TinyPors.Core.Repository;
using TinyPors.Core.Services.Models;

namespace TinyPors.Core.Services {
    public interface IServiceBase<T> where T : class {
        ServiceActionResult<T> Create(T model);
        ServiceActionResult<T> Update(T model, object id);
        ServiceActionResult<T> Delete(T model);
        ServiceActionResult<T> SoftDelete(T model);
        List<T> FindAll();
        List<T> Query(Expression<Func<T, bool>> expression);
        Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression);
        T Single(object id);
        Task<T> SingleAsync(object id);
        bool Validate();
        IQueryable<T> Select();
    }

    public class ServiceBase<T> : IServiceBase<T> where T : class {

        protected IValidationResult _validationResult;
        protected IRepository<T> _repository;

        public ServiceBase(IRepository<T> repository, IValidationResult validationResult) {
            _validationResult = validationResult;
            _repository = repository;
        }

        public virtual ServiceActionResult<T> Create(T model) {
            var result = new ServiceActionResult<T>();
            if (Validate()) {
                _repository.AddAsync(model);
                result.Entity = model;
                result.Success = true;
            }
            return result;
        }

        public virtual ServiceActionResult<T> Delete(T model) {
            var result = new ServiceActionResult<T> { Success = true };
            _repository.Delete(model);
            return result;
        }

        public virtual List<T> FindAll() {
            return new List<T>(_repository.FindAll());
        }

        public virtual List<T> Query(Expression<Func<T, bool>> expression) {
            return new List<T>(_repository.Query(expression));
        }

        public virtual async Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression) {
            return await _repository.QueryAsync(expression);
        }

        public virtual T Single(object id) {
            return _repository.Single(id);
        }

        public virtual async Task<T> SingleAsync(object id) {
            return await _repository.SingleAsync(id);
        }

        public virtual ServiceActionResult<T> SoftDelete(T model) {
            _repository.SoftDeleteAsync(model);
            return new ServiceActionResult<T> {
                Success = true,
                Entity = model
            };
        }

        public virtual ServiceActionResult<T> Update(T model, object id) {
            _repository.Update(model, id);
            return new ServiceActionResult<T> { Success = true };
        }

        public virtual bool Validate() {
            return _validationResult.IsValid;
        }

        public virtual IQueryable<T> Select() {
            return _repository.Select();
        }
    }
}
