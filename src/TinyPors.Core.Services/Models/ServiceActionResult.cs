﻿namespace TinyPors.Core.Services.Models {
    public class ServiceActionResult<T> where T : class {
        public bool Success { get; set; }
        public T Entity { get; set; }
        public object Id { get; set; }
        public string Message { get; set; }
    }
}
