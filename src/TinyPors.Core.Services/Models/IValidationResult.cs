﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinyPors.Core.Services.Models {
    public interface IValidationResult {
        void AddError(string key, string errorMessage);
        bool IsValid { get; }
    }

    public class ValidationResult : IValidationResult {

        private Dictionary<string, string> _errors;

        public ValidationResult() {
            _errors = new Dictionary<string, string>();
        }

        public bool IsValid { get; set; }

        public void AddError(string key, string errorMessage) {
            _errors.Add(key, errorMessage);
        }
    }
}
