﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TinyPors.Core.Domain.Infrastructure;

namespace TinyPors.Core.Repository {
    /// <summary>
    /// Generic Repository over Entities to provide basic database operations on Entities
    /// </summary>
    /// <typeparam name="T">The Entity Type</typeparam>
    public class GenericRepository<T> : IRepository<T> where T : class {
        private readonly IUnitOfWork _uow;
        public DbSet<T> Table;

        public GenericRepository(IUnitOfWork uow) {
            _uow = uow;
            Table = _uow.Set<T>();
        }

        public virtual void Delete(T entity) {
            Table.Remove(entity);
        }

        public virtual IQueryable<T> FindAll() {
            return Table;
        }


        public virtual T Single(object id) {
            return Table.Find(id);
        }

        public virtual async Task<T> SingleAsync(object id) {
            return await Table.FindAsync(id);
        }

        public virtual Task<int> SoftDelete(T entity) {
            throw new NotImplementedException();
        }

        public virtual async Task<T> AddAsync(T entity) {
            Table.Add(entity);
            await SaveAsync();
            return entity;
        }

        public virtual T Add(T entity) {
            Table.Add(entity);
            Save();
            return entity;
        }

        public virtual async Task<T> UpdateAsync(T entity, object id) {
            if (entity == null) return null;
            T exist = Table.Find(id);
            if (exist != null) {
                _uow.Entry<T>(exist).CurrentValues.SetValues(entity);
                await SaveAsync();
            }
            return exist;
        }

        public virtual T Update(T entity, object id) {
            if (entity == null) return null;
            T exist = Table.Find(id);
            if (exist != null) {
                _uow.Entry<T>(exist).CurrentValues.SetValues(entity);
                Save();
            }
            return exist;
        }


        public virtual async Task<bool> SoftDeleteAsync(T entity) {
            var entry = _uow.Entry(entity);
            entry.Property("IsDeleted").CurrentValue = true;
            entry.Property("IsDeleted").IsModified = true;
            await SaveAsync();
            return true;
        }

        public virtual async Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression) {
            return await Table.Where(expression).ToListAsync();
        }

        public ICollection<T> Query(Expression<Func<T, bool>> expression) {
            return Table.Where(expression).ToList();
        }

        public virtual async Task<int> SaveAsync() {
            return await _uow.SaveChangesAsync();
        }

        public virtual int Save() {
            return _uow.SaveChanges();
        }

        public virtual IQueryable<T> Select() {
            return Table.AsNoTracking().AsQueryable<T>();
        }

    }
}
