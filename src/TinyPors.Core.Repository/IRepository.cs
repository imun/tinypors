﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TinyPors.Core.Repository {
    public interface IRepository<T> where T : class {
        Task<T> AddAsync(T entity);
        T Add(T entity);
        Task<T> UpdateAsync(T entity, object id);
        T Update(T entity, object id);
        Task<bool> SoftDeleteAsync(T entity);
        void Delete(T entity);
        IQueryable<T> FindAll();
        Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression);
        ICollection<T> Query(Expression<Func<T, bool>> expression);
        T Single(object id);
        Task<T> SingleAsync(object id);
        int Save();
        Task<int> SaveAsync();
        IQueryable<T> Select();
    }
}
