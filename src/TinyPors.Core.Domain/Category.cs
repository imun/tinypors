﻿using System;
using System.Collections.Generic;
using System.Text;
using TinyPors.Core.Domain.Base;

namespace TinyPors.Core.Domain {
    public class Category: BaseEntity {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public string Slug { get; set; }
        public int? ParentId { get; set; }

        public ICollection<Question> Questions { get; set; }
    }
}
