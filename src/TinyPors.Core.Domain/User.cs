﻿using System;
using System.Collections;
using System.Collections.Generic;
using TinyPors.Core.Domain.Base;
namespace TinyPors.Core.Domain {
    public class User: BaseEntity {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public long TelegramId { get; set; }
        public string Ip { get; set; }
        public DateTime? RegisterDate { get; set; }

        #region Relations
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<UserAnswer> UserAnswers { get; set; }
        #endregion
    }
}
