﻿namespace TinyPors.Core.Domain {
    public class Tag {
        public int Id { get; set; }
        public string Title { get; set; }
        public int QuestionId { get; set; }
        public int UserId { get; set; }
    }
}
