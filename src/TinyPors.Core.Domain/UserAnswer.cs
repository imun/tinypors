﻿using System.ComponentModel.Design;
using TinyPors.Core.Domain.Base;
namespace TinyPors.Core.Domain {
    public class UserAnswer: BaseEntity {
        public long Id { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public string Email { get; set; }
        public string TextValue { get; set; }
        public int? UserId { get; set; }
        public string Ip { get; set; }
        public string UserAgent { get; set; }
        public virtual Question Question { get; set; }
        public virtual Answer Answer { get; set; }
        public virtual User User { get; set; }
    }

}
