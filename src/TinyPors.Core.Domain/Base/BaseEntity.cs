﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinyPors.Core.Domain.Base {
    public class BaseEntity {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Enable;
    }
}
