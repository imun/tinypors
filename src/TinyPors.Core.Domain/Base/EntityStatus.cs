﻿namespace TinyPors.Core.Domain.Base {
    public enum EntityStatus {
        Disable = 0,
        Enable = 1,
        Deleted = -1
    }
}
