﻿namespace TinyPors.Core.Domain.Base {
    public enum QuestionType {
        MultiWithOneAnswer = 0,
        MultiWithMultiAnswer = 1,
        Descriptive = 2
    }
}
