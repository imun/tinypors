﻿using System.Collections;
using System.Collections.Generic;
using TinyPors.Core.Domain.Base;

namespace TinyPors.Core.Domain {
    public class Answer: BaseEntity {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Body { get; set; }
        public bool IsCorrect { get; set; }
        public int UserId { get; set; }

        #region Relations
        public virtual User User { get; set; }
        public virtual Question Question { get; set; }
        public ICollection<UserAnswer> UserAnswers { get; set; }
        #endregion
    }
}
