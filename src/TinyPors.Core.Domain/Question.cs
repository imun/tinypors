﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TinyPors.Core.Domain.Base;
namespace TinyPors.Core.Domain {
    public class Question: BaseEntity {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int? CategoryId { get; set; }
        public int UserId { get; set; }
        public bool MultiSelect { get; set; }
        public bool DuplicateIp { get; set; }
        public string ImagePath { get; set; }
        public QuestionType QuestionType { get; set; } = QuestionType.MultiWithOneAnswer;
        public string Ip { get; set; }

        #region Relations
        public virtual User User { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<UserAnswer> UserAnswers { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        #endregion

    }
}
